﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Titanfall_2_Renamer {
    static class DataReader {
        // Privates
        static string[] lines;
        // Publics

        // Methods

        static public void search(string inFile, out List<string> tokens, out List<string> strings, out List<int> lineNums) {
            lines = System.IO.File.ReadAllLines(inFile);
            tokens = new List<string>();
            strings = new List<string>();
            lineNums = new List<int>();
            string[] split;

            Regex pattern = new Regex("\"WPN_\\w+\"");

            for(int i = 0; i < lines.Length; i++) {
                if (pattern.IsMatch(lines[i])) {
                    split = lines[i].Split('"');

                    //Token on split[1]
                    //string on split[3]
                    tokens.Add(split[1]);
                    strings.Add(split[3]);
                    lineNums.Add(i);
                }
            }
        }

        static public void write(List<string> tokens, List<string> strings,List<string> oldstrings, List<int> lineNums, string loc) {
            
            for(int i = 0; i < tokens.Count; i++) {
                lines[lineNums[i]] = lines[lineNums[i]].Replace('"' + oldstrings[i] + '"' , '"' + strings[i] + '"');
            }
            

            System.IO.File.WriteAllLines(loc, lines, Encoding.Unicode); // makes this compatible with source
        }
    }
}

