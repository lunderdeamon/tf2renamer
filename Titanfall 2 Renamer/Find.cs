﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Titanfall_2_Renamer {
    public partial class Find : Form {
        private string currDirectory = System.IO.Directory.GetCurrentDirectory();
        public UI parent;
        public Find() {
            InitializeComponent();
            openFileDialog1.InitialDirectory = currDirectory;
        }

        private void BttnOpen_Click(object sender, EventArgs e) {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e) {
            parent.r1Location = openFileDialog1.FileName;
            this.Close();
            parent.readAndPop();
            parent.BringToFront();
            parent.Activate();
        }
    }
}
