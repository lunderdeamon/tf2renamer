﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Titanfall_2_Renamer {
    public partial class UI : Form {
        private Find findBox = new Find();
        public string r1Location;
        List<string> tokens;
        List<string> strings;
        List<string> oldStrings;
        List<int> lineNums;

        public UI() {
            InitializeComponent();
            findBox.parent = this;
        }

        private void UI_Load(object sender, EventArgs e) {
            findBox.Show();
            findBox.BringToFront();
        }

        public void readAndPop() {
            //items = DataReader.populate(r1Location);
            DataReader.search(r1Location,out tokens,out strings, out lineNums);
            LsBoxToks.DataSource = tokens;
            oldStrings = new List<string>(strings);
        }

        private void LsBoxToks_DataSourceChanged(object sender, EventArgs e) {
            
        }
        private void LsBoxToks_SelectedIndexChanged(object sender, EventArgs e) {
            TxtBoxString.Text = strings[LsBoxToks.SelectedIndex];
        }

        private void BttnChange_Click(object sender, EventArgs e) {
            strings[LsBoxToks.SelectedIndex] = TxtBoxString.Text;
        }

        private void BttnFinish_Click(object sender, EventArgs e) {
            DialogResult sure = MessageBox.Show("Are you sure you're finished?", "Dumb Window Name Here", MessageBoxButtons.YesNo);
            if (sure == DialogResult.Yes) {
                DataReader.write(tokens, strings, oldStrings, lineNums, r1Location);
                MessageBox.Show("Ok, from here you need to go back to the VPK extraction tool and repack the folder that you output to. \r\nThis will create a pak000_000.vpk and pak000_dir.vpk.\r\nThose need to be renamed to client_frontend.bsp.pak000_000.vpk and englishclient_frontend.bsp.pak000_dir.vpk respectively", "Dumb Window Name Here", MessageBoxButtons.OK);
                this.Close();
                Environment.Exit(0);
            }
            else {

            }
        }

    }
}
