﻿namespace Titanfall_2_Renamer {
    partial class UI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.LsBoxToks = new System.Windows.Forms.ListBox();
            this.TxtBoxString = new System.Windows.Forms.TextBox();
            this.BttnFinish = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LsBoxToks
            // 
            this.LsBoxToks.FormattingEnabled = true;
            this.LsBoxToks.Location = new System.Drawing.Point(12, 12);
            this.LsBoxToks.Name = "LsBoxToks";
            this.LsBoxToks.Size = new System.Drawing.Size(326, 589);
            this.LsBoxToks.TabIndex = 1;
            this.LsBoxToks.SelectedIndexChanged += new System.EventHandler(this.LsBoxToks_SelectedIndexChanged);
            this.LsBoxToks.DataSourceChanged += new System.EventHandler(this.LsBoxToks_DataSourceChanged);
            // 
            // TxtBoxString
            // 
            this.TxtBoxString.Location = new System.Drawing.Point(344, 12);
            this.TxtBoxString.Name = "TxtBoxString";
            this.TxtBoxString.Size = new System.Drawing.Size(365, 20);
            this.TxtBoxString.TabIndex = 2;
            this.TxtBoxString.TextChanged += new System.EventHandler(this.BttnChange_Click);
            // 
            // BttnFinish
            // 
            this.BttnFinish.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.BttnFinish.Location = new System.Drawing.Point(344, 448);
            this.BttnFinish.Name = "BttnFinish";
            this.BttnFinish.Size = new System.Drawing.Size(365, 153);
            this.BttnFinish.TabIndex = 4;
            this.BttnFinish.Text = "Finished?";
            this.BttnFinish.UseVisualStyleBackColor = true;
            this.BttnFinish.Click += new System.EventHandler(this.BttnFinish_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(458, 299);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Search feature coming soonTM";
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 618);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BttnFinish);
            this.Controls.Add(this.TxtBoxString);
            this.Controls.Add(this.LsBoxToks);
            this.Name = "UI";
            this.Text = "Titanfall 2 Renamer";
            this.Load += new System.EventHandler(this.UI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox LsBoxToks;
        private System.Windows.Forms.TextBox TxtBoxString;
        private System.Windows.Forms.Button BttnFinish;
        private System.Windows.Forms.Label label1;
    }
}

